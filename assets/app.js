/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.css';
let formTodo = document.querySelector('form');
let listTodo = document.querySelector('#list-todo');
console.log(formTodo);
    formTodo.addEventListener('submit', function (e) {
        e.preventDefault();
        fetch(this.action, {
            body: new FormData(e.target),
            method: 'POST'
        })
            .then(response => response.json())
            .then(json => {
                handleResponse(json);
            });
    });



const handleResponse = function (response) {
    removeErrors();
    switch(response.code) {
        case 'TODO_ADDED_SUCCESSFULLY':
            listTodo.innerHTML += response.html;
            break;
        case 'TODO_INVALID_FORM':
            handleErrors(response.errors);
            break;
    }
}
const removeErrors = function() {
    const invalidFeedbackElements = document.querySelectorAll('.invalid-feedback');
    const isInvalidElements = document.querySelectorAll('.is-invalid');

    invalidFeedbackElements.forEach(errorElement => errorElement.remove());
    isInvalidElements.forEach(isInvalidElement => isInvalidElement.classList.remove('is-invalid'));
}
const handleErrors = function(errors) {
    if (errors.length === 0) return;

    for (const key in errors) {
        let element = document.querySelector(`#video_${key}`);
        element.classList.add('is-invalid');

        let div = document.createElement('div');
        div.classList.add('invalid-feedback', 'd-block');
        div.innerText = errors[key];

        element.after(div);
    }
}

// start the Stimulus application

import './bootstrap';


