<?php

namespace App\Services;

use App\Entity\Todo;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Twig\Environment;

class CallApiService
{
    private $client;
    private $environment;

    public function __construct(HttpClientInterface $client,
    Environment $environment
    )
    {
        $this->client = $client;
        $this->environment =$environment;
    }

    public function getUsersData(): array
    {
        $response = $this->client->request(
            'GET',
            'https://jsonplaceholder.typicode.com/users'
        );

        return $response->toArray();
    }
    public function getUserById(int $id){
        $response = $this->client->request(
            'GET',
            'https://jsonplaceholder.typicode.com/users?id=' .$id
        );
        return $response->toArray();
    }
    public function getPostsByUser(int $id){
        $response = $this->client->request(
            'GET',
            'https://jsonplaceholder.typicode.com/posts?userId=' .$id
        );
        return $response->toArray();
    }
    public function getPostById(int $id){
        $url= $id==0 ? 'https://jsonplaceholder.typicode.com/posts' : 'https://jsonplaceholder.typicode.com/posts?id=' .$id;
        $response = $this->client->request(
            'GET',
            $url
        );
        return $response->toArray();
    }
    public function getNombreUsers(): int{
        $response = $this->client->request(
            'GET',
            'https://jsonplaceholder.typicode.com/users'
        );

        return count($response->toArray());
    }
    public function getNombrePosts(): int{
        $response = $this->client->request(
            'GET',
            'https://jsonplaceholder.typicode.com/posts'
        );

        return count($response->toArray());
    }
    public function updateUser($body,int $id){
        $response = $this->client->request(
            'PUT',
            'https://jsonplaceholder.typicode.com/users/' .$id,[
                'body' =>$body
        ]

        );

        return count($response->toArray());

    }
    public function addTodo($body){
        $response = $this->client->request(
            'POST',
            'https://jsonplaceholder.typicode.com/todos',[
                'json' => $body
            ]

        );

        return count($response->toArray());
    }
    public function getNombreComments(): int{
        $response = $this->client->request(
            'GET',
            'https://jsonplaceholder.typicode.com/comments'
        );

        return count($response->toArray());
    }
    public function getTodosByUserId(int $id){
        $response = $this->client->request(
            'GET',
            'https://jsonplaceholder.typicode.com/todos?userId=' .$id
        );
        return $response->toArray();
}
    private function handleValidForm(FormInterface $todoForm) : JsonResponse
    {
        $todo = $todoForm->getData();
        $this->addTodo($todo);
        return new JsonResponse([
            'code' => Todo::TODO_ADDED_SUCCESSFULLY,
            'html' => $this->environment->render('home/todo.html.twig', [
                'todo' => $todo
            ])
        ]);
    }
    public function handleTodoFormData(FormInterface $todoForm): JsonResponse
    {
       if ($todoForm->isValid()) {
            return $this->handleValidForm($todoForm);
        } else {
            return $this->handleInvalidForm($todoForm);
        }
    }
    private function handleInvalidForm(FormInterface $todoForm) : JsonResponse
    {
        return new JsonResponse([
            'code' => Todo::TODO_INVALID_FORM,
            'errors' => $this->getErrorMessages($todoForm)
        ]);
    }
    private function getErrorMessages(FormInterface $form): array
    {
        $errors = [];

        foreach ($form->getErrors() as $error) {
            $errors[] = $error->getMessage();
        }

        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $errors[$child->getName()] = $this->getErrorMessages($child);
            }
        }

        return $errors;
    }
}
