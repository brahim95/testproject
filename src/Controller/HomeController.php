<?php

namespace App\Controller;

use App\Entity\Todo;
use App\Form\TodoType;
use App\Repository\TodoRepository;
use App\Services\CallApiService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class HomeController extends AbstractController
{
    /**
     * @Route("", name="app_homepage")
     */
    public function index(CallApiService $callApiService): Response
    {
        return $this->render('home/index.html.twig', [
            'nbrUsers' => $callApiService->getNombreUsers(),
            'nbrPosts' => $callApiService->getNombrePosts(),
            'nbrComments' => $callApiService->getNombreComments(),
        ]);
    }
    /**
     * @Route("/allusers", name="app_users")
     */
    public function users(CallApiService $callApiService): Response
    {
        return $this->render('home/users.html.twig', [
            'data' => $callApiService->getUsersData(),
        ]);
    }
    /**
     * @Route("/posts/{id}", name="app_posts")
     */
    public function posts(CallApiService $callApiService,$id): Response
    {
        return $this->render('home/posts.html.twig', [
            'data' => $callApiService->getPostsByUser($id),
        ]);
    }
    /**
     * @Route("/todos/{id}", name="app_todos")
     */
    public function todos(RequestStack $requestStack,CallApiService $callApiService,$id): Response
    {
        $request = $requestStack->getMainRequest();
        $todo= new Todo();
        $todo->setUserId($id);
        $todo->setCompleted(false);
        $todoForm=$this->createForm(TodoType::class,$todo);
        $todoForm->handleRequest($request);
        if($todoForm->isSubmitted()){
           return $callApiService->handleTodoFormData($todoForm);
        }
        return $this->render('home/todos.html.twig', [
            'data' => $callApiService->getTodosByUserId($id),
            'form' => $todoForm->createView()
        ]);
    }
    /**
     * @Route("/allposts/{id}", name="app_all_posts")
     */
    public function allposts(CallApiService $callApiService,$id): Response
    {
        return $this->render('home/posts.html.twig', [
            'data' => $callApiService->getPostById($id),
        ]);
    }
    /**
     * @Route("/user/{id}", name="app_user")
     */
    public function user(CallApiService $callApiService,$id,Request $request): Response
    {
        if($request->getMethod() == 'POST')
        {
            $callApiService->updateUser($request->request(),$id);
        }
        return $this->render('home/profile.html.twig', [
            'data' => $callApiService->getUserById($id)[0],
        ]);
    }
}
